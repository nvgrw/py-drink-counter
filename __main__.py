from flask import Flask, Response, request, url_for, redirect, make_response, render_template, abort, redirect
from pymongo import MongoClient
import os
from fauth import requires_auth

import config
import store
import twitter_ep

_doc_root = "/dc"
def _base_path(path):
    return _doc_root + path
app = Flask(__name__, static_folder=_base_path("/static"))
from werkzeug.contrib.fixers import ProxyFix
app.wsgi_app = ProxyFix(app.wsgi_app)

client = MongoClient("mongodb://localhost:27017")
db = client.drinkc
co_types, co_users = db.types, db.users

def _determine_is_add():
    return request.remote_addr in config.add_whitelist

@app.context_processor
def processor():
    def jround(value, amount):
        return round(value, amount)
    return dict(jround=jround)

@app.route(_base_path("/"))
def view_data():
    serveTwitter = True if request.args.get("t") == "1" else False
    return render_template("data.html", title="Data", serveTwitter=serveTwitter, isAdd=_determine_is_add(), docroot=_doc_root)

@app.route(_base_path("/add/"), methods=["GET", "POST"])
def add_drink():
    if _determine_is_add():
        if request.method == "GET":
            lastSucc = True if request.args.get("s") == "1" else False if request.args.get("s") == "0" else None
            hasSucc = "s" in request.args
            return render_template("add.html", title="Add", isAdd=True, items=store.get_drinks(co_types), success=lastSucc, hsuccess=hasSucc, docroot=_doc_root)
        elif request.method == "POST":
            success = store.add_drink(request.form, co_types)
            return redirect(url_for("add_drink", s=(1 if success else 0)))
    else:
        abort(403)

@app.route(_base_path("/breakdown/"), methods=["GET"])
def breakdown_drinks():
    return render_template("add.html", title="Breakdown", isAdd=False, items=store.get_drinks(co_types), success=False, hsuccess=False, docroot=_doc_root)

@app.route(_base_path("/create/"), methods=["GET", "POST"])
@requires_auth
def create_drink():
    if request.method == "GET":
        return render_template("create.html", title="Create", isAdd=_determine_is_add(), docroot=_doc_root)
    elif request.method == "POST":
        if ("name" in request.form and
            "volume" in request.form and
            "vunit" in request.form and
            "calories" in request.form and
            "pervol" in request.form and
            "cunit" in request.form):
            success = store.create_drink(request.form, co_types)
            return success
        else:
            abort(412)

@app.route(_base_path("/endpoint/brief.json"))
def endpoint_brief():
    return store.get_data(co_types, True)

@app.route(_base_path("/endpoint/data.json"))
def endpoint_data():
    return store.get_data(co_types)

@app.route(_base_path("/endpoint/twitter.json"))
def endpoint_twitter():
    start = 0
    if request.args.get("start") != None:
        try:
            start = abs(int(request.args.get("start")))
        except:
            pass
    return twitter_ep.get_tweets(start)

if __name__ == "__main__":
    app.run(host=config.host, port=config.port, debug=config.debug)