$(document).ready(function(){

    var drinkCount = $("#drink-count");
    var calorieCount = $("#calorie-count");

    function round(number, amount){
        amount = Math.pow(10, amount);
        return Math.round(number * amount) / amount;
    }

    function handleData(data){
        drinkCount.html(data.drinks);
        calorieCount.html(round(data.calories, 2));

        var rows = $(".row");
        var popularVolume = 0;
        var popularCount = 0;
        for(var x = 0; x < rows.length; x++){
            var children = $(rows[x]).children();
            for(var y = 0; y < children.length; y++){
                var child = $(children[y]);
                if(x < rows.length - 1){
                    if(child.hasClass("data-name")){
                        child.html(data.popular[x].name);
                    }else if(child.hasClass("data-count")){
                        child.html("&times;"+data.popular[x].count)
                        popularCount += data.popular[x].count;
                    }else if(child.hasClass("data-volume")){
                        child.html(round(data.popular[x].volume/1000, 2) + "L");
                        popularVolume += data.popular[x].volume;
                    }
                }else{
                    if(child.hasClass("data-name")){
                        child.html("Other");
                    }else if(child.hasClass("data-count")){
                        child.html("&times;"+(data.drinks-popularCount))
                    }else if(child.hasClass("data-volume")){
                        child.html(round((data.volume - popularVolume)/1000, 2) + "L");
                    }
                }
            }
        }
    }

    function tick(){
        $.ajax({
            dataType : "json",
            url : "/dc/endpoint/data.json",
            success : handleData,
            error : function(e){
                alert("Unable to fetch the latest data.\nPlease check your internet connection and/or refresh the page.")
            }
        });
    }

    tick();
    var timer = setInterval(tick, 10000);
});