$(document).ready(function(){
    var lastTime = 0;
    var template = $("#template-tweet");

    function handleData(data){
        if(data.length > 0){
            lastTime = data[0].time+1;
        }

        for(var i = 0; i < data.length; i++){
            var index = data.length - i - 1;
            var cloned = template.clone().attr("id", "");
            cloned.find("img.tweet-image").attr("src", data[index].profile_image);
            cloned.find("span.actual-username").html(data[index].username);
            cloned.find("span.realname").html(data[index].name);
            cloned.find("div.tweet-contents").html(data[index].text);
            cloned.find("div.tweet-time").html(moment.unix(data[index].time).format("MMM Do hh:mma"));
            cloned.find("a.tweet-profile-link").attr("href", "http://twitter.com/" + data[index].username);
            cloned.prependTo(".tweet-container");
        }
        var container = $(".tweet-container")
        while(container.children().length > 15) {
            container.children().last().remove()
        }
    }

    function tick(){
        $.ajax({
            dataType : "json",
            url : "/dc/endpoint/twitter.json?start=" + lastTime,
            success : handleData,
            error : function(e){
                alert("Unable to fetch the latest tweets.\nPlease check your internet connection and/or refresh the page.")
            }
        });
    }

    tick();
    var timer = setInterval(tick, 10000);
});