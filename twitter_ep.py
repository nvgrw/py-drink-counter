from TwitterAPI import TwitterAPI
from datetime import datetime
from threading import Timer

import config
import json
from collections import deque

twapi = TwitterAPI(config.consumer_key, config.consumer_secret, config.access_token_key, config.access_token_secret)
tweets = deque(maxlen=30)

def periodic_tweet_cache_update(query):
    request = twapi.request("search/tweets", {"q":query, "result_type":"recent", "count":30})
    for item in request.get_iterator():
        time = int(datetime.strptime(item["created_at"], "%a %b %d %H:%M:%S %z %Y").strftime("%s"))

        htmltext = item["text"]

        if "url" in item["entities"]:
            for url in item["entities"]["url"]["urls"]:
                htmltext = htmltext.replace(url["url"], "<a class=\"tweet-link\" href=\"%s\" target=\"_blank\">%s</a>" % (url["url"], url["display_url"]))
        if "urls" in item["entities"]:
            for url in item["entities"]["urls"]:
                htmltext = htmltext.replace(url["url"], "<a class=\"tweet-link\" href=\"%s\" target=\"_blank\">%s</a>" % (url["url"], url["display_url"]))

        if "hashtags" in item["entities"]:
            for ht in item["entities"]["hashtags"]:
                htmltext = htmltext.replace("#" + ht["text"], "<a class=\"tweet-hashtag\" href=\"https://twitter.com/search?q=%23"+ht["text"]+"\" target=\"_blank\">#%s</a>" % ht["text"])

        if "user_mentions" in item["entities"]:
            for um in item["entities"]["user_mentions"]:
                htmltext = htmltext.replace("@" + um["screen_name"], "<a class=\"tweet-link\" href=\"https://twitter.com/"+um["screen_name"]+"\" target=\"_blank\">@%s</a>" % um["screen_name"])

        tweets.append({
            "profile_image" : item["user"]["profile_image_url"],
            "username" : item["user"]["screen_name"],
            "name" : item["user"]["name"],
            "text" : htmltext,
            "time" : time
        })

    Timer(10, periodic_tweet_cache_update, [query]).start()

periodic_tweet_cache_update("#YRS2014 OR #YRSFoC")

def get_tweets(start):
    returnarr = []
    for item in tweets:
        if item["time"] < start:
            continue
        returnarr.append(item)
    return json.dumps(returnarr)