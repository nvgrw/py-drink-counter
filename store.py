from bson.objectid import ObjectId
import json

def create_drink(form, co_types):
    if not (form.get("volume").isdigit() and form.get("vunit").isdigit() and
        form.get("calories").isdigit() and form.get("pervol").isdigit() and form.get("cunit").isdigit()):
        return False

    entry = {
        "name" : form.get("name"),
        "volume" : unitToMl(float(form.get("volume")), float(form.get("vunit"))),
        "calories" :float(form.get("calories")) / unitToMl(float(form.get("pervol")), float(form.get("cunit"))),
        "count" : 0
    }
    co_types.insert(entry)
    return repr(True)

volumeMap = {
    0 : 1,
    1 : 1000,
    2 : 29.5735296,
    3 : 568.261485,
    4 : 284
}

def unitToMl(number, unit):
    if volumeMap[unit] is not None:
        return number * volumeMap[unit]
    else:
        return 0

def get_drinks(co_types):
    ting = co_types.find().sort("count", -1)
    return ting

def add_drink(form, co_types):
    if "id" not in form:
        return False
    cursor = co_types.find({"_id" : ObjectId(form.get("id"))}).limit(1);
    if cursor.count() > 0:
        co_types.update({"_id" : ObjectId(form.get("id"))}, {"$inc" : {"count" : 1}})
        return True
    else:
        return False

def get_data(co_types, brief=False):
    out = {
        "calories" : 0,
        "calories_per_drinks" : 0,
        "drinks" : 0,
        "volume" : 0,
        "volume_per_drinks" : 0,
        "popular" : []
    }

    for item in co_types.find():
        out["calories"] += item["calories"] * item["volume"] * item["count"]
        out["calories_per_drinks"] += item["calories"] * item["volume"]
        out["drinks"] += item["count"]
        out["volume"] += item["volume"] * item["count"]
        out["volume_per_drinks"] += item["volume"]

    if not brief:
        for item in co_types.find().sort("count", -1).limit(3):
            out["popular"].append({
                "name" : item["name"],
                "calories" : item["calories"] * item["volume"] * item["count"],
                "calories_per_drink" : item["calories"] * item["volume"],
                "calories_per_ml" : item["calories"],
                "volume" : item["volume"] * item["count"],
                "volume_per_drink" : item["volume"],
                "count" : item["count"]
            })
    else:
        del out["popular"]

    return json.dumps(out)