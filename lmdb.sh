mongod --config /usr/local/etc/mongod.conf &
dbpid=$!
echo "~~~~~ SERVER PID IS $dbpid"
sleep 1
mongo
kill -9 $dbpid
